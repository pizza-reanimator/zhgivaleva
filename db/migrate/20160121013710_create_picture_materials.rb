class CreatePictureMaterials < ActiveRecord::Migration
  def change
    create_table :picture_materials do |t|
      t.references :picture, index: true
      t.references :material, index: true

      t.timestamps null: false
    end
  end
end