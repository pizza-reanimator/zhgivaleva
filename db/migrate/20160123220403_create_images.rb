class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.text :url
      t.string :name
      t.float :width
      t.float :height
      t.integer :size

      t.timestamps null: false
    end
  end
end
