class CreateExhibitions < ActiveRecord::Migration
  def change
    create_table :exhibitions do |t|
      t.date :date
      t.string :title
      t.string :place

      t.timestamps null: false
    end
  end
end
