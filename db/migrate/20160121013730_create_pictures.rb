class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :title
      t.integer :year
      t.integer :width
      t.integer :height
      t.references :image, index: true
      t.references :tag, index: true

      t.timestamps null: false
    end
  end
end
