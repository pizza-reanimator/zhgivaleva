Rails.application.routes.draw do
	devise_for :admin_users, ActiveAdmin::Devise.config
	ActiveAdmin.routes(self)

	post "/(.:format)", to: "zhgivaleva#more"
	get  "/exhibitions", to: "zhgivaleva#exhibitions"
	get  "/about", to: "zhgivaleva#about"
	get  "/contact", to: "zhgivaleva#contact"

	root :to => "zhgivaleva#index"
end