class Picture < ActiveRecord::Base
	belongs_to :tag
	belongs_to :image
	has_many :picture_materials
	has_many :materials, :through => :picture_materials
	accepts_nested_attributes_for :picture_materials, :allow_destroy => true
	accepts_nested_attributes_for :materials, :allow_destroy => true
	accepts_nested_attributes_for :image, :allow_destroy => true

	STORE_DIR = "upload"
	STORE_PATH = File.join("public", STORE_DIR)

	def file= file
		img_type = file.original_filename.split(".").last

		fname = DateTime.now.strftime("%Q") + '.' + img_type
		path = File.join(STORE_PATH, fname)

		File.open(path, "wb") { |f| f.write(file.read) }

		image_opt = ImageOptim.new
		image_opt.optimize_image!(path)

		sizes = FastImage.size(path)

		image = Image.new
		image.url = File.join("/", STORE_DIR, fname)
		image.name = file.original_filename
		image.width = sizes[0]
		image.height = sizes[1]
		image.size = File.size(path)
		image.save

		self.image_id = image.id
	end
end