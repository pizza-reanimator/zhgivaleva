ActiveAdmin.register Exhibition do

	menu label: I18n.t("zhgivaleva.admin.exhibitions"), priority: 2

	permit_params :date, :title, :place

	index :title => I18n.t("zhgivaleva.admin.exhibitions") do
		selectable_column
		column I18n.t("zhgivaleva.admin.date"), :date
		column I18n.t("zhgivaleva.admin.title"), :title, :sortable => :title do |exhibition|
			link_to exhibition.title, [:edit_admin, exhibition]
		end
		column I18n.t("zhgivaleva.admin.place"), :place
	end

	filter :date, label: I18n.t("zhgivaleva.admin.date")
	filter :title, label: I18n.t("zhgivaleva.admin.title")
	filter :place, label: I18n.t("zhgivaleva.admin.place")

	form do |f|
		f.inputs I18n.t("zhgivaleva.admin.details") do
			f.input :date, label: I18n.t("zhgivaleva.admin.date"), as: :datepicker, datepicker_options: { min_date: "1995-1-1", max_date: "+3D" }
			f.input :title, label: I18n.t("zhgivaleva.admin.title")
			f.input :place, label: I18n.t("zhgivaleva.admin.place")
		end
		f.actions
	end

	controller do
		def update
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	
		def create
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	end

end