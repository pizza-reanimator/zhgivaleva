ActiveAdmin.register Tag do

	menu label: I18n.t("zhgivaleva.admin.tags"), priority: 4

	permit_params :title, :name

	index :title => I18n.t("zhgivaleva.admin.tags") do
		selectable_column
		column I18n.t("zhgivaleva.admin.title"), :title, :sortable => :title do |tag|
			link_to tag.title, [:edit_admin, tag]
		end
		column I18n.t("zhgivaleva.admin.name"), :name
	end

	config.filters = false

	form do |f|
		f.inputs I18n.t("zhgivaleva.admin.details") do
			f.input :title, label: I18n.t("zhgivaleva.admin.title")
			f.input :name, label: I18n.t("zhgivaleva.admin.name")
		end
		f.actions
	end

	controller do
		def update
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	
		def create
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	end

end