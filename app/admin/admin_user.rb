ActiveAdmin.register AdminUser do

	menu label: I18n.t("zhgivaleva.admin.admins"), priority: 6

	permit_params :email, :password, :password_confirmation

	index :title => I18n.t("zhgivaleva.admin.admins") do
		selectable_column
		column I18n.t("zhgivaleva.admin.email"), :email, :sortable => :email do |su|
			link_to su.email, [:edit_admin, su]
		end
	end

	config.filters = false

	form do |f|
		f.inputs I18n.t("zhgivaleva.admin.details") do
			f.input :email, label: I18n.t("zhgivaleva.admin.email")
			f.input :password, label: I18n.t("zhgivaleva.admin.password")
			f.input :password_confirmation, label: I18n.t("zhgivaleva.admin.password_confirmation")
		end
		f.actions
	end

	controller do
		def update
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	
		def create
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	end

end