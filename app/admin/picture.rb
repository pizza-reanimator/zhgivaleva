ActiveAdmin.register Picture do

	menu label: I18n.t("zhgivaleva.admin.pictures"), priority: 3

	permit_params :title, :year, :width, :height, :tag_id, :image_id, :file, :material_ids => []

	controller do
		belongs_to :tag, optional: true
	end

	index :title => I18n.t("zhgivaleva.admin.pictures") do
		selectable_column
		column I18n.t("zhgivaleva.admin.title"), :title, :sortable => :title do |pic|
			link_to pic.title, [:edit_admin, pic]
		end
		column I18n.t("zhgivaleva.admin.year"), :year
		column I18n.t("zhgivaleva.admin.tag"), :tag, :sortable => :tag do |pic|
			pic.tag.title
		end
		column I18n.t("zhgivaleva.admin.materials"), :materials do |pic|
			pic.materials.map { |material| material.title }.join(", ")
		end
	end

	filter :title, label: I18n.t("zhgivaleva.admin.title")
	filter :tag, label: I18n.t("zhgivaleva.admin.tag")
	filter :year, label: I18n.t("zhgivaleva.admin.year")
	filter :width, label: I18n.t("zhgivaleva.admin.width")
	filter :height, label: I18n.t("zhgivaleva.admin.height")
	filter :materials, label: I18n.t("zhgivaleva.admin.materials")

	form :html => { :enctype => "multipart/form-data" } do |f|
		f.inputs I18n.t("zhgivaleva.admin.details"), :multipart => true do
			f.input :title, label: I18n.t("zhgivaleva.admin.title")
			f.input :year, label: I18n.t("zhgivaleva.admin.year")
			f.input :width, label: I18n.t("zhgivaleva.admin.width")
			f.input :height, label: I18n.t("zhgivaleva.admin.height")
			f.input :tag, label: I18n.t("zhgivaleva.admin.tag"), member_label: :title
			f.input :materials, as: :check_boxes, label: I18n.t("zhgivaleva.admin.materials")
			f.input :file, as: :file, :for => :image, label: I18n.t("zhgivaleva.admin.image")
		end
		f.actions
	end

	controller do
		def update
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	
		def create
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	end

end