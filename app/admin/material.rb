ActiveAdmin.register Material do

	menu label: I18n.t("zhgivaleva.admin.materials"), priority: 5

	permit_params :title

	index :title => I18n.t("zhgivaleva.admin.materials") do
		selectable_column
		column I18n.t("zhgivaleva.admin.title"), :title, :sortable => :title do |material|
			link_to material.title, [:edit_admin, material]
		end
	end

	config.filters = false

	form do |f|
		f.inputs I18n.t("zhgivaleva.admin.details") do
			f.input :title, label: I18n.t("zhgivaleva.admin.title")
		end
		f.actions
	end

	controller do
		def update
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	
		def create
			super do |success, failure|
				success.html { redirect_to collection_url }
			end
		end
	end

end