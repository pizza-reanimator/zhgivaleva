//= require jquery2
//= require lib/jquery.tmpl
//= require lib/jquery.matchHeight
//= require lib/jquery.shuffle.modernizr
//= require lib/jquery.shuffle
//= require lib/lightbox
//= require twitter/bootstrap
//= require turbolinks

$ ->
	portfolio = $('#portfolio .wrap').shuffle {
		itemSelector: ".item"
		delimeter: ','
	}

	portfolio.on 'loading.shuffle', ->
		console.log 'loading.shuffle'

	portfolio.on 'done.shuffle', ->
		console.log 'done.shuffle'

	portfolio.on 'layout.shuffle', ->
		portfolio.shuffle 'update'

	portfolio.on 'removed.shuffle', ->
		console.log 'removed.shuffle'

	$('#portfolio .filter a').click (e) ->
		e.preventDefault()

		link = $(this)
		filter = link.data 'group'

		portfolio.shuffle 'shuffle', filter

		$('#portfolio > .filter li').removeClass 'active'
		
		link.parent().addClass 'active'

	$('#portfolio #more.btn').click (e) ->
		e.preventDefault()

		btn = $(this)

		$.post '/',
			offset: $('#portfolio .wrap .item').length
			(data) ->
				items = $('#picture_tpl')
							.tmpl data.items
							.appendTo '#portfolio .wrap'
				
				portfolio.shuffle 'appended', items
				
				if data.more is false
					btn.hide()

	lightbox.option {
		resizeDuration: 200
		wrapAround: true
	}

	$('.equal-height [class*="col-"]').matchHeight {
		byRow: false
	}