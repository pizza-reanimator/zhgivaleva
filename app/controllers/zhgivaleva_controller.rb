class ZhgivalevaController < ApplicationController

	Event = Struct.new(:day, :title, :place)
	MonthEvents = Struct.new(:name, :exhibitions)
	YearEvents = Struct.new(:name, :months)
	Painting = Struct.new(:tag, :title, :year, :materials, :url, :width, :height, :css)
	
	def index
		@tags = Tag.all
		pictures = Picture.all.limit(6)
		@paintings = Array.new

		pictures.each do |picture|
			ratio = picture.image.width.to_f / picture.image.height.to_f
			css = ""

			if ratio > 1.4
				css = "col-xs-12 col-sm-12 col-md-8 col-lg-6 col-el-4"
			else
				css = "col-xs-12 col-sm-6 col-md-4 col-lg-3 col-el-2"
			end

			@paintings.push(
				Painting.new(
					picture.tag.name,
					picture.title,
					picture.year,
					picture.materials.map { |material| material.title }.join(", "),
					picture.image.url,
					picture.width,
					picture.height,
					css
				)
			)
		end
	end

	def more
		offset = params[:offset].to_i
		limit = 6
		more = Picture.count - (offset + limit) > 0
		pictures = Picture.all.limit(limit).offset(offset)
		paintings = Array.new

		pictures.each do |picture|
			ratio = picture.image.width.to_f / picture.image.height.to_f
			css = ""

			if ratio > 1.4
				css = "col-xs-12 col-sm-12 col-md-8 col-lg-6 col-el-4"
			else
				css = "col-xs-12 col-sm-6 col-md-4 col-lg-3 col-el-2"
			end

			paintings.push(
				Painting.new(
					picture.tag.name,
					picture.title,
					picture.year,
					picture.materials.map { |material| material.title }.join(", "),
					picture.image.url,
					picture.width,
					picture.height,
					css
				)
			)
		end

		render json:
			Struct.new(:items, :more).new(paintings, more)
	end

	def exhibitions
		@events = Array.new
		exhibitions = Exhibition.all.order("date DESC")

		exhibitions.group_by{ |e| e.date.beginning_of_year }.each do |y, exs|
			months = Array.new
			
			exs.group_by{ |e| e.date.beginning_of_month }.each do |m, exs|
				events = Array.new

				exs.each do |e|
					events.push(
						Event.new(
							e.date.strftime("%d"),
							e.title,
							e.place
						)
					)
				end

				months.push(
					MonthEvents.new(
						m.strftime("%B"),
						events
					)
				)
			end
 
			@events.push(
				YearEvents.new(
					y.strftime("%Y"),
					months
				)
			)
		end

		puts @events
	end

	def about
		#
	end

	def contact
		#
	end

end